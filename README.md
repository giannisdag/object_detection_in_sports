### Εγκατάσταση συστήματος

τρέχουμε το 
`pip install -r requirements.txt`

εγκαθιστούμε τα:

`git clone https://github.com/tensorflow/models.git`

`git clone https://github.com/cocodataset/cocoapi.git`

Γενικότερα ακολουθούμε τις οδηγίες για εγκατάσταση του tensorflow object detection api

https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/installation.md

if object-detection not found, go to models/research and run
 
 `python3 setup.py install`
 
 Για να ενεργοποιηθεί το python path, πρέπει να το βάλουμε στο venv/bin/activate σύμφωνα με τις παρακάτω οδηγίες:
 https://stackoverflow.com/questions/4757178/how-do-you-set-your-pythonpath-in-an-already-created-virtualenv
 