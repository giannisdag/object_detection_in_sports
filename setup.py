from __future__ import division
import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
from collections import defaultdict
from io import StringIO
from PIL import Image
import cv2
from models.research.object_detection.utils import label_map_util
from models.research.object_detection.utils import visualization_utils as vis_util

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")
PATH_TO_CKPT = ''
PATH_TO_LABELS = ''
NUM_CLASSES = 0
LABEL = ''
video_output_filename = ''
selection = input("Επιλέξτε μοντέλο (1 ή 2): \n"
                 "(1 => pretrained  SSD Inception V2 coco, 2 => custom trained SSD Inception V2 coco)  ")
if selection == '1':
    PATH_TO_CKPT = 'model/frozen_inference_graph_ssd_inception_v2_coco.pb'
    # List of the strings that is used to add correct label for each box.
    PATH_TO_LABELS = 'model/mscoco_label_map.pbtxt'
    NUM_CLASSES = 90
    LABEL = 'person'
    video_output_filename = 'soccer_out'
elif selection == '2':
    PATH_TO_CKPT = 'training-soccer/trained-inference-graphs/frozen/frozen_inference_graph.pb'
    # List of the strings that is used to add correct label for each box.
    PATH_TO_LABELS = 'training-soccer/annotations/label_map.pbtxt'
    NUM_CLASSES = 1
    LABEL = 'player'
    video_output_filename = 'custom_soccer_out'




detection_graph = tf.Graph()
with detection_graph.as_default():
  od_graph_def = tf.GraphDef()
  with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
    serialized_graph = fid.read()
    od_graph_def.ParseFromString(serialized_graph)
    tf.import_graph_def(od_graph_def, name='')

label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)
def count_nonblack_np(img):
    """Return the number of pixels in img that are not black.
    img must be a Numpy array with colour values along the last axis.

    """
    return img.any(axis=-1).sum()


def detect_team(image, show=False):
    # define the list of boundaries
    boundaries = [
        # ([17, 15, 100], [50, 56, 200]), #red
        ([41, 197, 192], [81, 237, 232], [113, 71, 33], [153, 111, 73]),  # yellow-blue-2
        # ([25, 146, 190], [96, 174, 250]) #yellow
        ([198, 173, 132], [238, 213, 172], [188, 215, 176], [228, 255, 216])  # lightBlue-white-2
    ]
    i = 0
    for (lower1, upper1, lower2, upper2) in boundaries:
        # create NumPy arrays from the boundaries
        lower1 = np.array(lower1, dtype="uint8")
        upper1 = np.array(upper1, dtype="uint8")
        lower2 = np.array(lower2, dtype="uint8")
        upper2 = np.array(upper2, dtype="uint8")

        # find the colors within the specified boundaries and apply
        # the mask for first color
        mask1 = cv2.inRange(image, lower1, upper1)
        output1 = cv2.bitwise_and(image, image, mask=mask1)
        tot_pix1 = count_nonblack_np(image)
        color_pix1 = count_nonblack_np(output1)
        ratio1 = color_pix1 / tot_pix1

        # find the colors within the specified boundaries and apply
        # the mask for second color
        mask2 = cv2.inRange(image, lower2, upper2)
        output2 = cv2.bitwise_and(image, image, mask=mask2)
        tot_pix2 = count_nonblack_np(image)
        color_pix2 = count_nonblack_np(output2)
        ratio2 = color_pix2 / tot_pix2
        #         print("ratio is:", ratio)
        if (ratio1 > 0.01 or ratio2 > 0.01) and i == 0:
            return 'blue'
        elif (ratio1 > 0.01 or ratio2 > 0.01) and i == 1:
            return 'white'
        if show:
            cv2.imshow("image1"+str(i), np.hstack([image, output1]))
            cv2.imshow("image2"+str(i), np.hstack([image, output2]))
            if cv2.waitKey(0) & 0xFF == ord('q'):
                cv2.destroyAllWindows()

        i += 1

    return 'not_sure'


# %%
## To View Color Mask
# filename = 'image2.jpg'
filename = 'data/yellow-white-1.jpg'
# filename = 'yellow-white-2.jpg'
# filename = 'yellow-white-3.jpg'
image = cv2.imread(filename)
resize = cv2.resize(image, (640, 360))
detect_team(resize, show=True)

# intializing the web camera device

# from google.colab.patches import cv2_imshow
# % matplotlib
# inline
from matplotlib import pyplot as plt

out = cv2.VideoWriter(video_output_filename+'.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (640, 360))

filename = 'data/braz_arg_sample.mp4'
cap = cv2.VideoCapture(filename)

# Find OpenCV version
(major_ver, minor_ver, subminor_ver) = (cv2.__version__).split('.')

if int(major_ver) < 3:
    fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
    print
    "Frames per second using video.get(cv2.cv.CV_CAP_PROP_FPS): {0}".format(fps)
else:
    fps = cap.get(cv2.CAP_PROP_FPS)
    print
    "Frames per second using video.get(cv2.CAP_PROP_FPS) : {0}".format(fps)

# Running the tensorflow session
with detection_graph.as_default():
    with tf.Session(graph=detection_graph) as sess:
        counter = 0
        while (True):
            ret, image_np = cap.read()
            counter += 1
            if ret:
                h = image_np.shape[0]
                w = image_np.shape[1]

            if not ret:
                break
            if counter % 1 == 0:
                # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                image_np_expanded = np.expand_dims(image_np, axis=0)
                image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                # Each box represents a part of the image where a particular object was detected.
                boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                # Each score represent how level of confidence for each of the objects.
                # Score is shown on the result image, together with the class label.
                scores = detection_graph.get_tensor_by_name('detection_scores:0')
                classes = detection_graph.get_tensor_by_name('detection_classes:0')
                num_detections = detection_graph.get_tensor_by_name('num_detections:0')
                # Actual detection.
                (boxes, scores, classes, num_detections) = sess.run(
                    [boxes, scores, classes, num_detections],
                    feed_dict={image_tensor: image_np_expanded})
                # Visualization of the results of a detection.
                vis_util.visualize_boxes_and_labels_on_image_array(
                    image_np,
                    np.squeeze(boxes),
                    np.squeeze(classes).astype(np.int32),
                    np.squeeze(scores),
                    category_index,
                    use_normalized_coordinates=True,
                    line_thickness=3,
                    min_score_thresh=0.6)

                frame_number = counter
                loc = {}
                for n in range(len(scores[0])):
                    if scores[0][n] > 0.60:
                        # Calculate position
                        ymin = int(boxes[0][n][0] * h)
                        xmin = int(boxes[0][n][1] * w)
                        ymax = int(boxes[0][n][2] * h)
                        xmax = int(boxes[0][n][3] * w)

                        # Find label corresponding to that class
                        for cat in categories:
                            if cat['id'] == classes[0][n]:
                                label = cat['name']

                        ## extract every person
                        if label == LABEL:
                            # crop them
                            crop_img = image_np[ymin:ymax, xmin:xmax]
                            color = detect_team(crop_img)
                            if color != 'not_sure':
                                coords = (xmin, ymin)
                                if color == 'blue':
                                    loc[coords] = 'BRA'
                                else:
                                    loc[coords] = 'ARG'

                ## print color next to the person
                for key in loc.keys():
                    text_pos = str(loc[key])
                    cv2.putText(image_np, text_pos, (key[0], key[1] - 20), cv2.FONT_HERSHEY_SIMPLEX, 0.50, (255, 0, 0),
                                2)  # Text in black

            cv2.imshow('image', image_np)
            out.write(image_np)

            if cv2.waitKey(10) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                cap.release()
                break