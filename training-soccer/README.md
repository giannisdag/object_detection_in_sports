### Οδηγίες για training custom object detector
https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html

#### Βήμα 1
ετοιμάζω τα δεδομένα κάνοντας labeling


#### Βήμα 2
Δημιουργούμε τα αντίστοιχα .csv (train/test)
αρχικά πάμε στον φάκελο scripts/preprocessing

`python xml_to_csv.py -i ../../training-soccer/images/test/ -o ../../training-soccer/annotations/test_labels.csv`

ανάλογα για το train
#### Βήμα 3

Δημιουργούμε τα tfRecords (train/test)

`python generate_tfrecord.py --label=player --csv_input=../../training-soccer/annotations/train_labels.csv --img_path=../../training-soccer/images/train  --output_path=../../training-soccer/annotations/train.record`

ανάλογα για το test

#### Βήμα 4

κοιτάμε να έχει το σωστό PYTHONPATH που να βλέπει σωστά στο slim (για σιγουριά πάμε στον φάκελο research και δίνουμε export PYTHONPATH=\`pwd\`:\`pwd\`/slim)

τρέχουμε την εντολή για το train από τον φάκελο training-soccer

`python train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/ssd_inception_v2_coco.config`